////////////////////////////////////////////////////////////////////////
// Simple C++ OO program to simulate a simple Dice Game (solution 1)
// hacky
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>	//for time used in random number routines
#include <iostream>	//for cin >> & cout <<
#include <string>	//for string routines
using namespace std;

/*
//--------Dice class (version 1)
class Dice
{
	public:
		void init();
		int getFace();
		void roll();
	private:
		int face_;				//the number on the dice
		int getRandomValue( int);	//support function to get a random value
		void seed();	//support function to reset the random number generator
};

void Dice::init()
//reset the random number generator from current system time
{
	seed();
}
int Dice::getFace()
//get the value of the dice face
{
	return face_;
}
void Dice::roll()
//roll the dice
{
	face_ = getRandomValue( 6) + 1;
}
void Dice::seed()
//seeds the random number generator from current system time
//so that the numbers will be different every time
{
	srand( (unsigned)time( NULL ) );
}
int Dice::getRandomValue( int max)
//produce a random number in range [1..max]
{
	return ( rand() % max) + 1;
}
//--------end of Dice class (version 1)
*/
//--------RandomNumberGenerator class
class RandomNumberGenerator
{
	public:
		void seed() const;
		int getRandomValue(int) const;
};

void RandomNumberGenerator::seed() const
//seed the random number generator from current system time
//so that the numbers will be different every time
{
	srand(static_cast<unsigned>(time(0)));
}
int RandomNumberGenerator::getRandomValue(int max) const
//produce a random number in range [1..max]
{
	return (rand() % max) + 1;
}
//--------end of RandomNumberGenerator class

//--------Dice class (version 2)
class Dice
{
	public:
		void init();
		int getFace() const;
		void roll();
	private:
		int face_;		//the number on the dice
		RandomNumberGenerator rng_; 	//the private random number generator
};

void Dice::init()
//reset the random number generator from current system time
{
	rng_.seed();
}
int Dice::getFace() const
//get the value of the dice face
{
	return face_;
}
void Dice::roll()
//roll the dice
{
	face_ = rng_.getRandomValue( 6) + 1;
}
//--------end of Dice class (version 2)


//--------Score class
class Score
{
	public:
		void init();
		int getAmount() const;
		void updateAmount(int) ;
	private:
		int amount_;
};
void Score::init()
{
	amount_ = 0;
}
int Score::getAmount() const
{
	return amount_;
}
void Score::updateAmount(int value)
//increment when value>0, decrement otherwise
{
	amount_ += value;
}
//--------end of Score class



//---------------------------------------------------------------------------
//with one dice
/*
int main()
{
	void seed();
	int readInNumberOfThrows();
	int getRandomValue( int);
	Score score;
	score.init();
	const int number = readInNumberOfThrows();
	Dice dice;
	dice.init();
	score.init();
	cout << "\nThe original score is: ";
	cout << score.getAmount() << endl;
	for( int i = 1; i <= number; i++)
	{
		dice.roll();
		cout << endl << "In try #" << i << "\tdice value was: " << dice.getFace();
		if ( dice.getFace() == 6)
			score.updateAmount( 1);
	}
	cout << "\n\nThe final score is: ";
	cout << score.getAmount() << endl << endl;
	system("pause");	//hold screen open
	return 0;
}
*/
/*
//---------------------------------------------------------------------------
//with two dices
int main()
{
	int readInNumberOfThrows();

	Score score;
	score.init();

	const int number = readInNumberOfThrows();
	cout << "\nThe original score is: ";
	cout << score.getAmount() << endl;

	Dice dice1, dice2;
	dice1.init(); 
	dice2.init();

	for( int i = 1; i <= number; i++)
	{
		dice1.roll();
		dice2.roll();
		cout << "\nIn try no: "<< i << " \tdice values are: "
		     << dice1.getFace() << " & "
		     << dice2.getFace();
		if (dice1.getFace() == dice2.getFace())
		{
			score.updateAmount(dice1.getFace());
			if (dice1.getFace() == 6)
			{
				cout << "JackPot!!!";
				score.updateAmount(score.getAmount());
			}
		}
		else
			if ((dice1.getFace() == 6) || (dice2.getFace() == 6))
					score.updateAmount(1);
		cout << "\tThe current score is: "
		     << score.getAmount();	//check current score
	}
	cout << "\n\nThe final score is: ";
	cout << score.getAmount() << endl << endl;		//show final score
	system("pause");	//hold screen open
	return 0;
}

int readInNumberOfThrowsOfThrows()
//ask the user for the number of dice throws - assume positive value
{
	int num;
	cout << "\nHow many go do you want? ";
	cin >> num;
	return num;
}

*/
class Player {
private: 
	string name_;
	Score score_;
	string readInName() const;
public:
	void init();
	void updateScoreAmount(int a);
	string getName() const;
	int getScoreAmount() const;	
	int readInNumberOfThrows() const;
};

void Player::init() {
	score_.init();
	name_ = readInName();
}
void Player::updateScoreAmount(int a) {
	score_.updateAmount(a);
}
string Player::getName() const {
	return name_;
}
int Player::getScoreAmount() const {
	return score_.getAmount();
}
string Player::readInName() const {
//ask the user for their name
	string name;
	cout << "\nYour name? ";
	cin >> name;
	return name;
}
int Player::readInNumberOfThrows() const {
//ask the user for the number of dice throws
	int num;
	cout << "\nHow many go do you want? ";
	cin >> num;
	return num;
}


class Game {
private: 
	Dice dice1_;
	Dice dice2_;
public:
	void init();
	int play(int goes);
};
void Game::init() {
	dice1_.init();
	dice2_.init();
}
int Game::play(int goes) {
	Score score;
	score.init();
	for (int i = 1; i <= goes; i++)
	{
		dice1_.roll();
		dice2_.roll();
		cout << "\nIn try no: " << i << " \tdice values are: "
			<< dice1_.getFace() << " & "
			<< dice2_.getFace();
		if (dice1_.getFace() == dice2_.getFace())
		{
			score.updateAmount(dice1_.getFace());
			if (dice1_.getFace() == 6)
			{
				cout << "JackPot!!!";
				score.updateAmount(score.getAmount());
			}
		}
		else
			if ((dice1_.getFace() == 6) || (dice2_.getFace() == 6))
				score.updateAmount(1);
		cout << "\tThe current score is: "
		     << score.getAmount();	//check current score
	}
	return score.getAmount();
}

//---------------------------------------------------------------------------
//with Player and Game
int main() {	//main program
	Player player; 	//create the player
	player.init();	//get player's name and set up initial score
	const int goes(player.readInNumberOfThrows());
	Game game; 	//create the game
	game.init();	//set up the new dice
	player.updateScoreAmount(game.play(goes));	
				//play the game and update the score of the player
	cout << "\n\n" << player.getName() << "'s final score is: "
	     << player.getScoreAmount()	//show final score
	     << endl << endl;
	system("pause");	//hold screen open
	return 0;
}

